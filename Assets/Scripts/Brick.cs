﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	
	public AudioClip crack, hit;
	public Sprite[] hitSprites;
	public static int breakableCount = 0;
	public GameObject smoke;
	
	private LevelManager levelManager;
	private int timesHit;
	private bool isBreakable; 

	// Use this for initialization
	void Start () {
		isBreakable = (this.tag == "Breakable");
		
		if (isBreakable) {
			breakableCount++;
			}
		timesHit = 0;
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnCollisionEnter2D (Collision2D col) {
		
		if (isBreakable) {
			HandleHits();
			}
		}
	
	void HandleHits () {
		timesHit++;
		int maxHits = hitSprites.Length + 1;
		if (timesHit >= maxHits) {
			AudioSource.PlayClipAtPoint(crack, transform.position);
			breakableCount --;
			levelManager.BrickDestroy();
			smokePuff();
			Destroy(gameObject);
			
		} else {
			AudioSource.PlayClipAtPoint(hit, transform.position);
			LoadSprites();
			
		}
	}
	
	void smokePuff() {
		GameObject smokePuff = Instantiate(smoke, gameObject.transform.position, Quaternion.identity) as GameObject;
		smokePuff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
	}
	
	void LoadSprites() {
		int spriteIndex = timesHit - 1;
		if (hitSprites[spriteIndex]) {
			this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
			}
		
	}
	
	// TODO Remove this method once we can actually win!
	void SimulateWin() {
		levelManager.LoadNextLevel();
		}
}