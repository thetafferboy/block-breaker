﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	public bool autoPlay = false;
	
	private Ball ball;
	
	// Use this for initialization
	void Start () {
	ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.F12))
			{
			toggleAutoplay();
			}
		
		if (!autoPlay) {
			MoveWithMouse();
			}
		else {
			cpuPlay();
			}
		}
	
	void toggleAutoplay() {
		if (!autoPlay) {
			autoPlay = true;
			}
		else {
			autoPlay = false;
			}
	}
	
	void cpuPlay() {
		Vector3 paddPos = new Vector3 (0.5f,this.transform.position.y,0f);
		Vector3 ballPos = ball.transform.position;
		paddPos.x = Mathf.Clamp(ballPos.x, 0.5f, 15.5f);
		this.transform.position = paddPos;
	}
		
	void MoveWithMouse() {
		float mousePosInBlocks = (Input.mousePosition.x / Screen.width * 16);
		Vector3 paddPos = new Vector3 (this.transform.position.x,this.transform.position.y,this.transform.position.z);
		paddPos.x = Mathf.Clamp (mousePosInBlocks,0.5f,15.5f);
		this.transform.position = paddPos;	
	}
}