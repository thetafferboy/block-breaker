﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name) {
        SceneManager.LoadScene(name);
	}
	
	public void QuitGame() {
		Debug.Log ("Quitting game...");
		Application.Quit ();
		}
		
	public void LoadNextLevel() {
            int Scene = SceneManager.GetActiveScene().buildIndex;
            if (Scene < SceneManager.sceneCountInBuildSettings)
                SceneManager.LoadScene(Scene + 1);
        
    }
		
	public void BrickDestroy() {
		if (Brick.breakableCount <= 0) {
			LoadNextLevel();
			}
		}
}
